import math

class Move():

    def __init__(self):
        self.distance_left = 0
        self.distance_right = 0
        self.speed_ratio = 0


class ForwardMove(Move):

    def __init__(self, distance):
        if distance < 0:
            raise ValueError("distance needs to be positive")

        self.distance_left = distance
        self.distance_right = distance
        self.speed_ratio = 1

    def __str__(self):
        return "Forward"

class BackwardsMove(Move):

    def __init__(self, distance):
        if distance < 0:
            raise ValueError("distance needs to be positive")

        self.distance_left = -distance
        self.distance_right = -distance
        self.speed_ratio = 1

    def __str__(self):
        return "Backwards"

class Turn(Move):
    CENTER_TO_WHEEL = 0.164 / 2  # distance from the center of the robot to the center of the wheels

    def __init__(self, direction, radius, degrees=90):
        if radius < self.CENTER_TO_WHEEL:
            raise ValueError("The radius can't be smaller than {}".format(self.CENTER_TO_WHEEL))
        elif degrees < 0 or degrees > 180:
            print("degrees= " + str(degrees))
            raise ValueError("degrees need to be between 0 and 180, got: ".format(degrees))

        d = 0
        if direction == "left":
            d = -self.CENTER_TO_WHEEL
        elif direction == "right":
            d = self.CENTER_TO_WHEEL
        else:
            raise ValueError("direction needs to be 'left' or 'right' not " + str(direction))

        self.distance_left = degrees * math.pi * (radius + d) / 180
        self.distance_right = degrees * math.pi * (radius - d) / 180
        self.direction = direction
        self.speed_ratio = None

        if self.distance_left > 0 and self.distance_right > 0:
            self.speed_ratio = self.distance_left / self.distance_right

    def __str__(self):
        return "Turn " + self.direction
