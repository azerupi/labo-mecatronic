import math
import RPi.GPIO as GPIO

class Encoder():
    PULSES_PER_ROTATION = 20
    WHEEL_DIAMETER = 0.07 # meters

    def __init__(self, pin, handler):
        self.pin = pin
        self.handler = handler
        self.pulses = 0
        self.rotations = 0
        GPIO.setup(pin, GPIO.IN)
        GPIO.add_event_detect(pin, GPIO.RISING, callback=self.handle_rising_edge)
    
    def handle_rising_edge(self, _pin):
        self.pulses += 1
        if self.pulses > self.PULSES_PER_ROTATION:
            self.rotations += 1
            self.pulses = 0
        
        distance = self.distance()

        self.handler(distance)
    
    def reset(self):
        self.pulses = 0
        self.rotations = 0
    
    def distance(self):
        return (self.rotations + float(self.pulses) / self.PULSES_PER_ROTATION) * math.pi * self.WHEEL_DIAMETER
