import sys
import time
import RPi.GPIO as GPIO
from movecontroller import MoveController
from rangesensor import RangeSensor
from move import ForwardMove, BackwardsMove, Turn

class Robot():
    
    def __init__(self):
        print("Initialising...")

        self.x = 0              # x cartesian coordinate in meters
        self.y = 0              # y cartesian cooridnate in meters
        self.orientation = 0    # orientation in degrees

        GPIO.setmode(GPIO.BOARD)
        self.movecontroller = MoveController()
        self.rangesensor = RangeSensor(8, 7)
        time.sleep(1)
        print("GO!")
    
    def run(self, moves):
        range_run = 0
        
        while True:
            if self.movecontroller.check_distance_reached():
                if len(moves) > 0:
                    self.movecontroller.move(moves.pop(0))
                else:
                    break

            if range_run % 10 == 0:
                obstacle = self.rangesensor.measure() < 0.15
            else:
                range_run += 1
            
            if obstacle:
                self.movecontroller.stop()
            else:
                self.movecontroller.rectify()

            time.sleep(0.01)

        self.stop()

    def stop(self):
        self.movecontroller.stop()
        GPIO.cleanup()
    
    def error(self, e=None):
        print("Going down, aaaargh!")
        if e is not None:
            print(e)


if __name__ == '__main__':
    robot = Robot()
    moves = [ForwardMove(1.0), Turn('right', 0.4), Turn('left', 0.3)]
    try:
        robot.run(moves)
    except KeyboardInterrupt:
        robot.stop()
        sys.exit(0)
    except Exception as e:
        robot.stop()
        robot.error()
        raise e

