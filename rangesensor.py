import math
import time
import RPi.GPIO as GPIO

class RangeSensor():
    
    def __init__(self, pin_trig, pin_echo):
        self.pin_trig = pin_trig
        self.pin_echo = pin_echo

        GPIO.setup(self.pin_trig, GPIO.OUT)
        GPIO.setup(self.pin_echo, GPIO.IN)

        GPIO.output(self.pin_trig, GPIO.LOW)

    def measure(self):
        GPIO.output(self.pin_trig, True)
        time.sleep(0.00001)
        GPIO.output(self.pin_trig, False)

        start = time.time()
        end = time.time()
        while GPIO.input(self.pin_echo) == 0:
            start = time.time()

        while GPIO.input(self.pin_echo) == 1:
            end = time.time()
        
        duration = end - start
        distance = duration * 171.50

        print("Distance " + str(distance) + "m")

        return distance
