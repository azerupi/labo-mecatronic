from motor import Motor
from encoder import Encoder
from move import ForwardMove, BackwardsMove, Turn

class MoveController():
    
    def __init__(self):
        self.distance_left = 0
        self.distance_right = 0
        self.current_move = None

        self.left_motor = Motor(pin_forward=23, pin_backwards=21, pin_enable=19)
        self.left_encoder = Encoder(37, self.handle_distance_left)

        self.right_motor = Motor(pin_forward=16, pin_backwards=18, pin_enable=22)
        self.right_encoder = Encoder(38, self.handle_distance_right)
    

    def move(self, move):
        self.current_move = move
        self.distance_left = 0
        self.distance_right = 0
        self.left_encoder.reset()
        self.right_encoder.reset()
        
        if move is None:
            self.stop()
            return
        
        print("---------------------------\nExecuting Move: " + str(move))

        if isinstance(move, ForwardMove):
            self.move_forward()
        elif isinstance(move, BackwardsMove):
            self.move_backwards()
        elif isinstance(move, Turn):
            self.move_turn(move)


    def rectify(self):
        if self.current_move.speed_ratio is None:
            return

        # If one of the wheels is stuck...
        if self.distance_left == 0:
            self.left_motor.forward(1)
            self.right_motor.stop()
            return
        elif self.distance_right == 0:
            self.right_motor.forward(1)
            self.left_motor.stop()
            return
        
        l = self.distance_left
        r = self.distance_right
        diff = abs(l-r)
        apply_left = l-r > 0

        correction = max(1, min(0, 1 - diff / 0.1))

        if isinstance(self.current_move, ForwardMove):
            if apply_left:
                self.left_motor.forward(correction)
                self.right_motor.forward()
            else:
                self.left_motor.forward()
                self.right_motor.forward(correction)
        elif isinstance(self.current_move, BackwardsMove):
            if apply_left:
                self.left_motor.backwards(correction)
                self.right_motor.backwards()
            else:
                self.left_motor.backwards()
                self.right_motor.backwards(correction)
        elif isinstance(self.current_move, Turn):
            advancement = (self.distance_left + self.distance_right) / (self.move.distance_left + self.move.distance_right)
            expected_diff = abs(self.move.distance_left - self.move.distance_right) * advancement
            diff -= expected_diff
            correction = max(1, min(0, 1 - diff / 0.1))
            if apply_left:
                self.left_motor.forward(correction)
                self.right_motor.forward()
            else:
                self.left_motor.forward()
                self.right_motor.forward(correction)

        # if self.distance_left < 0.1 and self.distance_right < 0.1:
        #     print("Skipping rectifying, distances are too small...")
        #     return

        # relative_speed_ratio = self.distance_left / self.distance_right

        # correction_ratio = relative_speed_ratio / self.current_move.speed_ratio

        # print("Rectifying trajectory,\n\tmeasured_speed_ratio={}\n\ttheoretical_speed_ratio={}\n\tcorrection_ratio={}".format(relative_speed_ratio, self.current_move.speed_ratio, correction_ratio))

        # if isinstance(self.current_move, ForwardMove) or isinstance(self.current_move, Turn):
        #     if correction_ratio > 1:
        #         self.left_motor.forward(1/correction_ratio)
        #         self.right_motor.forward(1)
        #     elif correction_ratio < 1:
        #         self.left_motor.forward(1)
        #         self.right_motor.forward(correction_ratio)
        # elif isinstance(self.current_move, BackwardsMove):
        #     if correction_ratio > 1:
        #         self.left_motor.backwards(1/correction_ratio)
        #         self.right_motor.backwards(1)
        #     elif correction_ratio < 1:
        #         self.left_motor.backwards(1)
        #         self.right_motor.backwards(correction_ratio)


    def check_distance_reached(self):
        left = False
        right = False
        if self.current_move is None:
            return True
        
        print("Distance traveled (left: {}, right: {})".format(self.distance_left, self.distance_right))

        if self.distance_left > abs(self.current_move.distance_left):
            self.left_motor.stop()
            left = True
        
        if self.distance_right > abs(self.current_move.distance_right):
            self.right_motor.stop()
            right = True
        
        return left and right


    def handle_distance_left(self, distance):
        self.distance_left = distance


    def handle_distance_right(self, distance):
        self.distance_right = distance


    def move_forward(self):
        self.left_motor.forward()
        self.right_motor.forward()


    def move_backwards(self):
        self.left_motor.backwards()
        self.right_motor.backwards()


    def move_turn(self, turn):
        left = 1 if turn.distance_left > 0 else 0
        right = 1 if turn.distance_right > 0 else 0

        # If one of the distances to turn is zero,
        # move the other wheel only
        if turn.distance_right == 0:
            return self.left_motor.forward()
        elif turn.distance_left == 0:
            return self.right_motor.forward()
        
        theoretical_speed_ratio = turn.distance_left / turn.distance_right

        if theoretical_speed_ratio > 1:
            self.left_motor.forward()
            self.right_motor.forward(1/theoretical_speed_ratio)
        else:
            self.left_motor.forward(theoretical_speed_ratio)
            self.right_motor.forward()


    def stop(self):
        self.left_motor.stop()
        self.right_motor.stop()
