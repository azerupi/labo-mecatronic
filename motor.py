import RPi.GPIO as GPIO

class Motor():
    
    MAX_SPEED = 0.4

    def __init__(self, pin_forward, pin_backwards, pin_enable):
        GPIO.setup(pin_forward, GPIO.OUT)
        GPIO.setup(pin_backwards, GPIO.OUT)
        GPIO.setup(pin_enable, GPIO.OUT)

        self.pin_forward = GPIO.PWM(pin_forward, 100)
        self.pin_backwards = GPIO.PWM(pin_backwards, 100)
        self.pin_enable = pin_enable

        self.pin_forward.start(0)
        self.pin_backwards.start(0)

    def forward(self, velocity=1):
        self.pin_forward.ChangeDutyCycle(velocity * 100 * self.MAX_SPEED)
        self.pin_backwards.ChangeDutyCycle(0)
        GPIO.output(self.pin_enable, GPIO.HIGH)
    
    def backwards(self, velocity=1):
        self.pin_forward.ChangeDutyCycle(0)
        self.pin_backwards.ChangeDutyCycle(velocity * 100 * self.MAX_SPEED)
        GPIO.output(self.pin_enable, GPIO.HIGH)

    def stop(self):
        GPIO.output(self.pin_enable, GPIO.LOW)
